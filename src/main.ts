import Vue from 'vue'
import App from './App.vue'
import VueMq from 'vue-mq';
import vuetify from './plugins/vuetify';
import '@/assets/styles/reset.scss';

Vue.config.productionTip = false

Vue.use(VueMq, {
  breakpoints: {
    sm: 450,
    md: 1250,
    lg: Infinity,
  }
})

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
